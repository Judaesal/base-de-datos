from pyfirmata import Arduino, util
from tkinter import *
from PIL import Image , ImageTk
placa = Arduino ('COM3')
it = util.Iterator(placa)
#inicio el iteratodr
it.start()

led = placa.get_pin('d:8:o')
led1= placa.get_pin('d:9:o')
led2= placa.get_pin('d:10:o')
led3= placa.get_pin('d:11:o')
cuenta=0
counter =0

ventana = Tk()
ventana.state('zoomed') #iniciar la pantalla maximizada
ventana.geometry('1200x800')
ventana.configure(bg = 'white')
ventana.title("Punto3")
draw = Canvas(ventana, width=1900, height=980)
draw.place(x = 80,y = 80)

led_draw=draw.create_oval(15,15,70,70,fill="white")
led1_draw=draw.create_oval(80,15,135,70,fill="white")
led2_draw=draw.create_oval(145,15,200,70,fill="white")

texto = Label(ventana, 
                text="LED", 
                bg='cadet blue1', 
                font=("Arial Bold", 25), 
                fg="white")
texto.grid(padx=20, pady=20,column=0, row=0)
def led_pm(valor):
    if(int(valor) > 10 and int(valor)<=30):
        update_label()
def update_label():
    global cuenta, counter
    counter += 1
    dato2['text'] = str(counter)
    if(int(counter)>10 and int(counter)<=25):
        led.write(1)
        led1.write(0)
        led2.write(0)
        draw.itemconfigure(led_draw, fill = "red")
        draw.itemconfigure(led1_draw, fill = "white")
        draw.itemconfigure(led2_draw, fill = "white")
    if(int(counter)>25 and int(counter)<=40):
        led.write(0)
        led1.write(1)
        led2.write(0)
        draw.itemconfigure(led_draw, fill = "white")
        draw.itemconfigure(led1_draw, fill = "red")
        draw.itemconfigure(led2_draw, fill = "white")
    if(int(counter)>40 and int(counter)<= 60):
        led.write(0)
        led1.write(0)
        led2.write(1)
        draw.itemconfigure(led_draw, fill = "white")
        draw.itemconfigure(led1_draw, fill = "white")
        draw.itemconfigure(led2_draw, fill = "red") 
        cuenta=your_label.after(500, update_label)

def pause():
    global cuenta
    dato2.after_cancel(cuenta)
lum = Scale(ventana, 
            command=led_pm, 
            from_= 0, 
            to = 30, 
            orient = HORIZONTAL, 
            length = 250,
            
            label = "INT",
            bg = 'cadet blue2',
            font=("Arial Bold", 15),
            fg="white"
            )
lum.grid(padx=400, pady=20,column=0, row=1)

your_label=Label(ventana)
start_button=Button(ventana,text="start",command=update_label)
start_button.place(x=120, y=230)
pause=Button(ventana,text="pause",command=pause)
pause.place(x=120, y=270)
stop=Button(ventana,text="parar",command=ventana.destroy)
stop.place(x=120, y=290)
dato2=Label(ventana, text="00", bg='cadet blue1', font=("Arial Bold", 14), fg="white")
dato2.place(x=120, y=210)
ventana.mainloop()
