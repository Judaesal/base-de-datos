from pyfirmata import Arduino, util
from tkinter import *
from PIL import Image , ImageTk

import firebase_admin
from firebase_admin import credentials
from firebase_admin import db


cred = credentials.Certificate("D:/Users/Juan/Desktop/Sergio Arboleda/Herramientas/parcial 2/oellave/oellave2.json")
firebase_admin.initialize_app(cred,{'databaseURL' : 'https://tkin-95561.firebaseio.com/'})


placa = Arduino ('COM3')
it = util.Iterator(placa)
#inicio el iteratodr
it.start()
led = placa.get_pin('d:8:o')
led1= placa.get_pin('d:9:o')
led2= placa.get_pin('d:10:o')
led3= placa.get_pin('d:11:o')
led4= placa.get_pin('d:12:o')
led5= placa.get_pin('d:13:o')
ventana = Tk()
ventana.geometry('850x850')
ventana.configure(bg = 'white')
ventana.title("punto 2")
draw = Canvas(ventana, width=700, height=700)
draw.place(x = 10,y = 10)

led_draw=draw.create_oval(15,15,70,70,fill="white")
led1_draw=draw.create_oval(80,15,135,70,fill="white")
led2_draw=draw.create_oval(145,15,200,70,fill="white")
led3_draw=draw.create_oval(205,15,265,70,fill="white")
led4_draw=draw.create_oval(270,15,330,70,fill="white")
led5_draw=draw.create_oval(340,15,400,70,fill="white")

def cargar(valor):
    referen=db.reference("elvalor")
    referen.update({
        'variable':{
            'losdatos':int(valor),
            }
        })
    

    
texto = Label(ventana, 
                text="LEDS", 
                bg='cadet blue1', 
                font=("Arial Bold", 25), 
                fg="white")
texto.grid(padx=600, pady=20,column=0, row=0)
def led12(valor):
    cargar(valor)
    if(int(valor) > 0 and int(valor)<=20):
        led.write(1)
        led1.write(0)
        led2.write(0)
        led3.write(0)
        led4.write(0)
        led5.write(0)
        draw.itemconfigure(led_draw, fill = "red")
        draw.itemconfigure(led1_draw, fill = "white")
        draw.itemconfigure(led2_draw, fill = "white")
        draw.itemconfigure(led3_draw, fill = "white")
        draw.itemconfigure(led4_draw, fill = "white")
        draw.itemconfigure(led5_draw, fill = "white")
        
    if(int(valor) > 20 and int(valor)<=30):
        led.write(1)
        led1.write(1)
        led2.write(0)
        led3.write(0)
        led4.write(0)
        led5.write(0)
        draw.itemconfigure(led_draw, fill = "red")
        draw.itemconfigure(led1_draw, fill = "blue")
        draw.itemconfigure(led2_draw, fill = "white")
        draw.itemconfigure(led3_draw, fill = "white")
        draw.itemconfigure(led4_draw, fill = "white")
        draw.itemconfigure(led5_draw, fill = "white")
    if(int(valor) > 30 and int(valor)<=50):
        led.write(1)
        led1.write(1)
        led2.write(1)
        led3.write(0)
        led4.write(0)
        led5.write(0)
        draw.itemconfigure(led_draw, fill = "red")
        draw.itemconfigure(led1_draw, fill = "blue")
        draw.itemconfigure(led2_draw, fill = "yellow")
        draw.itemconfigure(led3_draw, fill = "white")
        draw.itemconfigure(led4_draw, fill = "white")
        draw.itemconfigure(led5_draw, fill = "white")
    if(int(valor) > 50 and int(valor)<=70):
        led.write(1)
        led1.write(1)
        led2.write(1)
        led3.write(1)
        led4.write(0)
        led5.write(0)
        draw.itemconfigure(led_draw, fill = "red")
        draw.itemconfigure(led1_draw, fill = "blue")
        draw.itemconfigure(led2_draw, fill = "yellow")
        draw.itemconfigure(led3_draw, fill = "green")
        draw.itemconfigure(led4_draw, fill = "white")
        draw.itemconfigure(led5_draw, fill = "white")
    if(int(valor) > 70 and int(valor)<=80):
        led.write(1)
        led1.write(1)
        led2.write(1)
        led3.write(1)
        led4.write(1)
        led5.write(0)
        draw.itemconfigure(led_draw, fill = "red")
        draw.itemconfigure(led1_draw, fill = "blue")
        draw.itemconfigure(led2_draw, fill = "yellow")
        draw.itemconfigure(led3_draw, fill = "green")
        draw.itemconfigure(led4_draw, fill = "red")
        draw.itemconfigure(led5_draw, fill = "white")
    if(int(valor) > 80 and int(valor)<=100):
        led.write(1)
        led1.write(1)
        led2.write(1)
        led3.write(1)
        led4.write(1)
        led5.write(1)
        draw.itemconfigure(led_draw, fill = "red")
        draw.itemconfigure(led1_draw, fill = "blue")
        draw.itemconfigure(led2_draw, fill = "yellow")
        draw.itemconfigure(led3_draw, fill = "green")
        draw.itemconfigure(led4_draw, fill = "red")
        draw.itemconfigure(led5_draw, fill = "blue")
    ventana.update()
    
lum = Scale(ventana, 
            command=led12, 
            from_= 0, 
            to = 100, 
            orient = VERTICAL, 
            length = 250, 
            label = "prender",
            bg = 'cadet blue2',
            font=("Arial Bold", 15),
            fg="white"
            )
lum.grid(padx=20, pady=20,column=0, row=1)
ventana.mainloop()
